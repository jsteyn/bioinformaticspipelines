#!/bin/bash -e
#$ -q bigmem.q
#$ -cwd -V
#$ -pe smp 1-5
#$ -l h_vmem=100G
#$ -M jannetta.steyn@newcastle.ac.uk
#$ -m a
#$ -N trinity
#$ -o /home/njss3/WORKING_DATA/MikeTaggart/A3193_cavPorTranscriptome/logs/trinity.$JOB_ID.$TASK_ID.out
#$ -e /home/njss3/WORKING_DATA/MikeTaggart/A3193_cavPorTranscriptome/logs/trinity.$JOB_ID.$TASK_ID.err

module load apps/bowtie/1.1.0
module load apps/samtools/1.3

start_time=$(date)
start_secs=$(date +%s)
echo "### Logging Info ###"
echo "### Job: ${JOB_ID}: ${JOB_NAME} ###"
echo "### Array ID: ${SGE_TASK_ID} ###"
echo "### Job Occupying ${NSLOTS} slots ###"
echo "### Job running on ${HOSTNAME} ###"
echo "### Started at: ${start_time} ###"
echo

source /home/njss3/WORKING_DATA/MikeTaggart/A3193_cavPorTranscriptome/scripts/variables.sh
readarray -t FILENAME < ${WORK}/scripts/samples.txt

FASTQ=`for f in ${WORK}/fastq_trimmed/*_R1.trimmed.fastq.gz;do echo -e "$f\c,";echo -e ",\c";done`
FASTQ1=${FASTQ%,}
FASTQ=`for f in ${WORK}/fastq_trimmed/*_R2.trimmed.fastq.gz;do echo -e "$f\c,";echo -e ",\c";done`
FASTQ2=${FASTQ%,}

BASENAME=${BASENAME%.fastq}

/home/njss3/trinityrnaseq-2.2.0/Trinity \
  --SS_lib_type RF \
  --seqType fq \
  --left ${FASTQ1} \
  --right ${FASTQ2} \
  --CPU ${NSLOTS} \
  --max_memory 100G \
  --output ${WORK}/trinity/

end_time=$(date)
end_secs=$(date +%s)
time_elapsed=$(echo "${end_secs} - ${start_secs}" | bc)
echo
echo "### Ended at: ${end_time} ###"
echo "### Time Elapsed: ${time_elapsed} ###"


