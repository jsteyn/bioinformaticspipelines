#!/bin/bash -e
###########################
# qsub switches
###########################
# $ -cwd
#$ -pe smp 1
#$ -l h_vmem=20G
#$ -N AAAAsortsam
#$ -hold_jid_ad AAAAbwa
#$ -o $HOME/MSc_practical/logs/sortsam.$JOB_ID.$TASK_ID.out
#$ -e $HOME/MSc_practical/logs/sortsam.$JOB_ID.$TASK_ID.err
###########################
# Load modules
###########################

module load apps/picard/1.130

source ${HOME}/MSc_practical/scripts/variables.sh
readarray -t FILENAME < ${WORK}/scripts/samples.txt

java -Xmx10g -jar ${PICARD_PATH}/picard.jar SortSam  VALIDATION_STRINGENCY=LENIENT INPUT=${WORK}sam/${FILENAME[$SGE_TASK_ID-1]}.sam OUTPUT=${WORK}bam/${FILENAME[$SGE_TASK_ID-1]}.bam SORT_ORDER=coordinate TMP_DIR=${TEMP_DIR}
echo "Script completed"
