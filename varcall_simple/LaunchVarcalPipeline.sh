#!/bin/sh -e

qsub -t $1-$1 var00_fastqc.sh
qsub -t $1-$1 var01_fastuniq.sh
qsub -t $1-$1 var02_bwa.sh
qsub -t $1-$1 var03_sortsam.sh
qsub -t $1-$1 var04_samindex.sh
qsub -t $1-$1 var05_freebayes.sh
qsub -t $1-$1 var06_vcfconv.sh
qsub -t $1-$1 var07_annovar.sh
qsub -t $1-$1 var08_bamstats.sh

