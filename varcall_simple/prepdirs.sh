#! /bin/sh -e
if [ ! -d "~/MSc_practical" ]; then
	mkdir ~/MSc_practical
	mkdir ~/MSc_practical/bam
	mkdir ~/MSc_practical/fastq
	mkdir ~/MSc_practical/fastqc
	mkdir ~/MSc_practical/fastquniq
	mkdir ~/MSc_practical/hg38
	mkdir ~/MSc_practical/logs
	mkdir ~/MSc_practical/sam
	mkdir ~/MSc_practical/scripts
	mkdir ~/MSc_practical/stats
	mkdir ~/MSc_practical/vcf
	mkdir ~/MSc_practical/vcf.ann
fi

