#!/bin/bash -e
###########################
# qsub switches
###########################
# $ -cwd
#$ -pe smp 1
#$ -l h_vmem=10G
#$ -N AAAAfastqc
#$ -o $HOME/MSc_practical/logs/fastqc.$JOB_ID.$TASK_ID.out
#$ -e $HOME/MSc_practical/logs/fastqc.$JOB_ID.$TASK_ID.err

module load apps/fastqc

source ${HOME}/MSc_practical/scripts/variables.sh
readarray -t FILENAME < ${WORK}/scripts/samples.txt

fastqc --t ${NSLOTS} -o ${WORK}fastqc ${FASTQFILES}/${FILENAME[$SGE_TASK_ID-1]}*.fastq 
echo "Script completed"
