#!/bin/bash
###########################
# qsub switches
###########################
# $ -cwd
#$ -pe smp 5-15
#$ -l h_vmem=10G
#$ -N AAAAannovar
#$ -o $HOME/MSc_practical/logs/annovar.$JOB_ID.out
#$ -e $HOME/MSc_practical/logs/annovar.$JOB_ID.err

module load apps/annovar/529/noarch

source ${HOME}/MSc_practical/scripts/variables.sh

###########################
# Run bwa mem
###########################
annotate_variation.pl -buildver hg38 -downdb -webfrom ucsc refGene ${WORK}hg38/

annotate_variation.pl -buildver hg38 -downdb cytoBand ${WORK}hg38/

annotate_variation.pl -buildver hg38 -downdb genomicSuperDups ${WORK}hg38/ 

annotate_variation.pl -buildver hg38 -downdb -webfrom ucsc esp6500siv2_all ${WORK}hg38/

annotate_variation.pl -buildver hg38 -downdb -webfrom ucsc 1000g2015aug ${WORK}hg38/

annotate_variation.pl -buildver hg38 -downdb -webfrom ucsc exac03 ${WORK}hg38/ 

annotate_variation.pl -buildver hg38 -downdb -webfrom ucsc avsnp147 ${WORK}hg38/ 

annotate_variation.pl -buildver hg38 -downdb -webfrom ucsc dbnsfp30a ${WORK}hg38/
echo "Script completed"
