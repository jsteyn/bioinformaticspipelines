#!/bin/bash -e
###########################
# qsub switches
###########################
# $ -cwd
#$ -pe smp 1
#$ -l h_vmem=10G
#$ -N AAAAvcfcon
#$ -hold_jid_ad AAAAfreebayes
#$ -o $HOME/MSc_practical/logs/vcfcon.$JOB_ID.$TASK_ID.out
#$ -e $HOME/MSc_practical/logs/vcfcon.$JOB_ID.$TASK_ID.err

module load apps/annovar/20150211

source ${HOME}/MSc_practical/scripts/variables.sh
readarray -t FILENAME < ${WORK}/scripts/samples.txt

convert2annovar.pl -format vcf4 \
	-includeinfo ${WORK}vcf/${FILENAME[$SGE_TASK_ID-1]}.vcf > ${WORK}vcf/${FILENAME[$SGE_TASK_ID-1]}.avinput

echo "Script completed"
