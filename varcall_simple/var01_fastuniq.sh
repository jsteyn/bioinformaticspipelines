#!/bin/bash -e
###########################
# qsub switches
###########################
# $ -cwd
#$ -pe smp 1
#$ -l h_vmem=70G
#$ -N AAAAfastuniq
#$ -hold_jid AAAAfastqc
#$ -o $HOME/MSc_practical/logs/fastuniq.$JOB_ID.$TASK_ID.out
#$ -e $HOME/MSc_practical/logs/fastuniq.$JOB_ID.$TASK_ID.err
###########################
# Load modules
###########################

module load apps/fastuniq/1.1

source ${HOME}/MSc_practical/scripts/variables.sh
readarray -t FILENAME < ${WORK}/scripts/samples.txt
###########################
# REMOVE DUPLICATES WITH fastuniq
###########################
FILELIST=${FILENAME[$SGE_TASK_ID-1]}.txt
ls ${FASTQFILES}/${FILENAME[$SGE_TASK_ID-1]}*_001.fastq > ${WORK}scripts/${FILELIST}
echo "fastuniq -i ${WORK}scripts/${FILENAME[$SGE_TASK_ID-1]}.txt -t q -o ${WORK}fastuniq/${FILENAME[$SGE_TASK_ID-1]}_R1.fastq -p ${WORK}fastuniq/${FILENAME[$SGE_TASK_ID-1]}_R2.fastq"
fastuniq -i ${WORK}scripts/${FILENAME[$SGE_TASK_ID-1]}.txt -t q -o ${WORK}fastuniq/${FILENAME[$SGE_TASK_ID-1]}_R1.fastq -p ${WORK}fastuniq/${FILENAME[$SGE_TASK_ID-1]}_R2.fastq
rm ${WORK}scripts/${FILENAME[$SGE_TASK_ID-1]}.txt

echo "Script completed"
