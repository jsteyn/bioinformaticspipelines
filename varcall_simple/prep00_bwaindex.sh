#!/bin/bash
###########################
# qsub switches
###########################
# $ -cwd
#$ -pe smp 5-15
#$ -l h_vmem=10G
#$ -N AAAAindex
#$ -o $HOME/MSc_practical/logs/index.$JOB_ID.out
#$ -e $HOME/MSc_practical/logs/index.$JOB_ID.err

module load apps/bwa/0.7.15

source ${HOME}/MSc_practical/scripts/variables.sh

###########################
# Run bwa mem
###########################
bwa index ${HOME}/WORKING_DATA/genomes/hg38/hg38.fa

echo "Script completed"
