#!/bin/bash -e
###########################
# qsub switches
###########################
# $ -cwd
#$ -pe smp 1
#$ -l h_vmem=10G
#$ -N AAAAannovar
#$ -hold_jid_ad AAAAvcfcon
#$ -o $HOME/MSc_practical/logs/annovar.$JOB_ID.out
#$ -e $HOME/MSc_practical/logs/annovar.$JOB_ID.err

module load apps/annovar/20150211

source ${HOME}/MSc_practical/scripts/variables.sh
readarray -t FILENAME < ${WORK}/scripts/samples.txt

echo "convert2annovar.pl -format vcf4 -includeinfo ${WORK}vcf/${FILENAME[$SGE_TASK_ID-1]}.vcf > ${WORK}vcf/${FILENAME[$SGE_TASK_ID-1]}.avinput"
convert2annovar.pl -format vcf4 \
	-includeinfo ${WORK}vcf/${FILENAME[$SGE_TASK_ID-1]}.vcf > ${WORK}vcf/${FILENAME[$SGE_TASK_ID-1]}.avinput

echo "table_annovar.pl ${WORK}vcf/${FILENAME[$SGE_TASK_ID-1]}.avinput /sharedlustre/IGM/For_Helen_from_Louise/annovar/humandb/ -buildver hg38 -out ${WORK}vcf.ann/${FILENAME[$SGE_TASK_ID-1]}.csv -remove -protocol knownGene,refGene,exac03,esp6500siv2_all,cosmic70,dbnsfp30a,dbnsfp31a_interpro,dbscsnv11,hrcr1,kaviar_20150923,nci60,mitimpact24 -operation g,g,f,f,f,f,f,f,f,f,f,f -nastring . -csvout"
table_annovar.pl ${WORK}vcf/${FILENAME[$SGE_TASK_ID-1]}.avinput /sharedlustre/IGM/For_Helen_from_Louise/annovar/humandb/ \
	-buildver hg38 \
	-out ${WORK}vcf.ann/${FILENAME[$SGE_TASK_ID-1]}.csv \
	-remove \
	-protocol knownGene,refGene,exac03,esp6500siv2_all,cosmic70,dbnsfp30a,dbnsfp31a_interpro,dbscsnv11,hrcr1,kaviar_20150923,nci60,mitimpact24 \
	-operation g,g,f,f,f,f,f,f,f,f,f,f \
	-nastring . \
	-csvout
echo "Script completed"
