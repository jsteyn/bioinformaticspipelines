#!/bin/bash -e
###########################
# qsub switches
###########################
# $ -cwd
#$ -pe smp 1
#$ -l h_vmem=10G
#$ -N AAAAsamindex
#$ -hold_jid_ad AAAAsortsam
#$ -o $HOME/MSc_practical/logs/samindex.$JOB_ID.$TASK_ID.out
#$ -e $HOME/MSc_practical/logs/samindex.$JOB_ID.$TASK_ID.err

module load apps/samtools/1.3.1

source ${HOME}/MSc_practical/scripts/variables.sh
readarray -t FILENAME < ${WORK}/scripts/samples.txt

samtools index ${WORK}bam/${FILENAME[$SGE_TASK_ID-1]}.bam

echo "Script completed"
