#!/bin/bash -e
###########################
# qsub switches
###########################
# $ -cwd
#$ -pe smp 1
#$ -l h_vmem=10G
#$ -N AAAAbwa
#$ -hold_jid_ad AAAAfastuniq
#$ -o $HOME/MSc_practical/logs/bwa.$JOB_ID.$TASK_ID.out
#$ -e $HOME/MSc_practical/logs/bwa.$JOB_ID.$TASK_ID.err

module load apps/bwa/0.7.15

source ${HOME}/MSc_practical/scripts/variables.sh
readarray -t FILENAME < ${WORK}/scripts/samples.txt

FILENAME_A=`ls ${WORK}fastuniq/${FILENAME[$SGE_TASK_ID-1]}_R1.fastq`
FILENAME_B=`ls ${WORK}fastuniq/${FILENAME[$SGE_TASK_ID-1]}_R2.fastq`
FLOWCELL="001"
SAMPLE_ID=${FILENAME[$SGE_TASK_ID-1]}
Library_ID="John.Sayer.NG-7935"
SEQ_PLATFORM="ILLUMINA"
###########################
# Run bwa mem
###########################
echo "bwa mem -R "@RG\tID:FlowCell.${FLOWCELL}\tSM:${SAMPLE_ID}\tPL:${SEQ_PLATFORM}\tLB${Library_ID}.${SAMPLE_ID}" -t ${NSLOTS} -M ${GENOME} ${FILENAME_A} ${FILENAME_B} > ${WORK}sam/${SAMPLE_ID}.sam"
bwa mem -R "@RG\tID:FlowCell.${FLOWCELL}\tSM:${SAMPLE_ID}\tPL:${SEQ_PLATFORM}\tLB${Library_ID}.${SAMPLE_ID}" -t ${NSLOTS} -M ${GENOME} ${FILENAME_A} ${FILENAME_B} > ${WORK}sam/${SAMPLE_ID}.sam
echo "Script completed"
