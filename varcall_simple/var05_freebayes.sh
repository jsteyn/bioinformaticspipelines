#!/bin/bash -e
###########################
# qsub switches
###########################
# $ -cwd
#$ -pe smp 1-5
#$ -l h_vmem=10G
#$ -N AAAAfreebayes
#$ -hold_jid_ad AAAAsamindex
#$ -o $HOME/MSc_practical/logs/freebayes.$JOB_ID.$TASK_ID.out
#$ -e $HOME/MSc_practical/logs/freebayes.$JOB_ID.$TASK_ID.err

start_time=$(date)
start_secs=$(date +%s)

module load apps/freebayes/1.0.2

source ${HOME}/MSc_practical/scripts/variables.sh
readarray -t FILENAME < ${WORK}/scripts/samples.txt

echo "freebayes -f ${GENOME} ${WORK}bam/${FILENAME[$SGE_TASK_ID-1]}.bam > ${WORK}vcf/${FILENAME[$SGE_TASK_ID-1]}.vcf"
freebayes -f ${GENOME} ${WORK}bam/${FILENAME[$SGE_TASK_ID-1]}.bam > ${WORK}vcf/${FILENAME[$SGE_TASK_ID-1]}.vcf
echo "Script completed"

end_time=$(date)
end_secs=$(date +%s)
time_elapsed=$(echo "${end_secs} - ${start_secs}" | bc)
echo
echo "### Ended at: ${end_time} ###"
echo "### Time Elapsed: ${time_elapsed} ###"
