#!/bin/bash -e
###########################
# qsub switches
###########################
# $ -cwd
#$ -pe smp 1
#$ -l h_vmem=10G
#$ -N AAAAbamstats
#$ -hold_jid_ad AAAAsortsam
#$ -o $HOME/MSc_practical/logs/bamstats.$JOB_ID.out
#$ -e $HOME/MSc_practical/logs/bamstats.$JOB_ID.err

module load apps/samtools/1.3.1

source ${HOME}/MSc_practical/scripts/variables.sh
readarray -t FILENAME < ${WORK}/scripts/samples.txt

samtools flagstat ${WORK}bam/${FILENAME[$SGE_TASK_ID-1]}.bam > ${WORK}stats/${FILENAME[$SGE_TASK_ID-1]}.bamstats


echo "Script completed"
