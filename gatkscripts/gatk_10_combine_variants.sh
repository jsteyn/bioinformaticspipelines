#!/bin/bash
#$ -cwd -V
#$ -pe smp 1
#$ -l h_vmem=50G
# $ -M email@email.com
#$ -m a
#$ -N workdir11
#$ -hold_jid workdir10
#$ -o /home/nsjc11/WORKING_DATA/workdir/logs/combine_variants.$JOB_ID.$TASK_ID.out
#$ -e /home/nsjc11/WORKING_DATA/workdir/logs/combine_variants.$JOB_ID.$TASK_ID.err

module add apps/java/jre-1.7.0_75
module add apps/gatk/3.3-protected

start_time=$(date)
start_secs=$(date +%s)
echo "### Logging Info ###"
echo "### Job: ${JOB_ID}: ${JOB_NAME} ###"
echo "### Array ID: ${SGE_TASK_ID} ###"
echo "### Job Occupying ${NSLOTS} slots ###"
echo "### Job running on ${HOSTNAME} ###"
echo "### Started at: ${start_time} ###"
echo

source workflow/gatk_variables.sh

GATK_JAVA_ARGS='-Xmx42g -Xms42g -XX:-UseLargePages' 

java ${GATK_JAVA_ARGS} -jar ${GATK_ROOT}/GenomeAnalysisTK.jar \
   -T CombineVariants \
   -R ${HG19} \
   --variant:Sample1 ${WORK}/vcf/Sample_1.PASS.vcf \
   --variant:Sample2 ${WORK}/vcf/Sample_2.PASS.vcf \
   --variant:Sample3 ${WORK}/vcf/Sample_3.PASS.vcf \
   --variant:Sample4 ${WORK}/vcf/Sample_4.PASS.vcf \
   --variant:Sample5 ${WORK}/vcf/Sample_5.PASS.vcf \
   --variant:Sample6 ${WORK}/vcf/Sample_6.PASS.vcf \
   --variant:Sample7 ${WORK}/vcf/Sample_7.PASS.vcf \
   --variant:Sample8 ${WORK}/vcf/Sample_8.PASS.vcf \
   --variant:Sample9 ${WORK}/vcf/Sample_9.PASS.vcf \
   --variant:Sample10 ${WORK}/vcf/Sample_10.PASS.vcf \
   -genotypeMergeOptions PRIORITIZE \
   -priority Sample1,Sample2,Sample3,Sample4,Sample5,Sample6,Sample7,Sample8,Sample9,Sample10 \
   -o ${WORK}/vcf/all_variants.vcf 
                      
end_time=$(date)
end_secs=$(date +%s)
time_elapsed=$(echo "${end_secs} - ${start_secs}" | bc)
echo
echo "### Ended at: ${end_time} ###"
echo "### Time Elapsed: ${time_elapsed} ###"
