#!/bin/bash
#$ -cwd -V
#$ -pe smp 10
#$ -l h_vmem=110G
# $ -M email@email.com
#$ -m a
#$ -N workdir7
#$ -hold_jid_ad workdir6
#$ -o /home/nsjc11/WORKING_DATA/workdir/logs/print_reads.$JOB_ID.$TASK_ID.out
#$ -e /home/nsjc11/WORKING_DATA/workdir/logs/print_reads.$JOB_ID.$TASK_ID.err

module add apps/java/jre-1.7.0_75
module add apps/gatk/3.3-protected

start_time=$(date)
start_secs=$(date +%s)
echo "### Logging Info ###"
echo "### Job: ${JOB_ID}: ${JOB_NAME} ###"
echo "### Array ID: ${SGE_TASK_ID} ###"
echo "### Job Occupying ${NSLOTS} slots ###"
echo "### Job running on ${HOSTNAME} ###"
echo "### Started at: ${start_time} ###"
echo

source workflow/gatk_variables.sh
BAM=${WORK}/bam/Sample_${SGE_TASK_ID}/Aligned_realigned.bam
BAMFILE=$(basename ${BAM})

# move bamfile and index to localscratch
cp ${BAM}* ${TMPDIR}

GATK_JAVA_ARGS='-Xmx96g -Xms96g -XX:-UseLargePages' 
java ${GATK_JAVA_ARGS} -jar ${GATK_ROOT}/GenomeAnalysisTK.jar \
   -T PrintReads \
   -nct 10 \
   -R ${HG19} \
   -I ${TMPDIR}/${BAMFILE} \
   -BQSR ${WORK}/recalibrator/Sample_${SGE_TASK_ID}.grp \
   -o ${WORK}/bam/Sample_${SGE_TASK_ID}/Aligned_recalibrated.bam
                      
end_time=$(date)
end_secs=$(date +%s)
time_elapsed=$(echo "${end_secs} - ${start_secs}" | bc)
echo
echo "### Ended at: ${end_time} ###"
echo "### Time Elapsed: ${time_elapsed} ###"
