#!/bin/bash
#$ -cwd -V
#$ -pe smp 1
#$ -l h_vmem=50G
# $ -M email@email.com
#$ -m a
#$ -N workdir10
#$ -hold_jid_ad workdir9
#$ -o /home/nsjc11/WORKING_DATA/workdir/logs/select_variants.$JOB_ID.$TASK_ID.out
#$ -e /home/nsjc11/WORKING_DATA/workdir/logs/select_variants.$JOB_ID.$TASK_ID.err

module add apps/java/jre-1.7.0_75
module add apps/gatk/3.3-protected

start_time=$(date)
start_secs=$(date +%s)
echo "### Logging Info ###"
echo "### Job: ${JOB_ID}: ${JOB_NAME} ###"
echo "### Array ID: ${SGE_TASK_ID} ###"
echo "### Job Occupying ${NSLOTS} slots ###"
echo "### Job running on ${HOSTNAME} ###"
echo "### Started at: ${start_time} ###"
echo

source workflow/gatk_variables.sh

GATK_JAVA_ARGS='-Xmx42g -Xms42g -XX:-UseLargePages' 
java ${GATK_JAVA_ARGS} -jar ${GATK_ROOT}/GenomeAnalysisTK.jar \
   -T SelectVariants \
   -R ${HG19} \
   --variant ${WORK}/vcf/Sample_${SGE_TASK_ID}.filtered.vcf \
   -select "vc.isNotFiltered()" \
   -o ${WORK}/vcf/Sample_${SGE_TASK_ID}.PASS.vcf 
                      
end_time=$(date)
end_secs=$(date +%s)
time_elapsed=$(echo "${end_secs} - ${start_secs}" | bc)
echo
echo "### Ended at: ${end_time} ###"
echo "### Time Elapsed: ${time_elapsed} ###"
