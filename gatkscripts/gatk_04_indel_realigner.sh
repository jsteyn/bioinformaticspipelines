#!/bin/bash
#$ -cwd -V
#$ -pe smp 10
#$ -l h_vmem=110G
# $ -M email@email.com
#$ -m a
#$ -N workdir5
#$ -hold_jid_ad workdir4
#$ -o /home/nsjc11/WORKING_DATA/workdir/logs/indel_realigner.$JOB_ID.$TASK_ID.out
#$ -e /home/nsjc11/WORKING_DATA/workdir/logs/indel_realigner.$JOB_ID.$TASK_ID.err

module add apps/java/jre-1.7.0_75
module add apps/gatk/3.3-protected

start_time=$(date)
start_secs=$(date +%s)
echo "### Logging Info ###"
echo "### Job: ${JOB_ID}: ${JOB_NAME} ###"
echo "### Array ID: ${SGE_TASK_ID} ###"
echo "### Job Occupying ${NSLOTS} slots ###"
echo "### Job running on ${HOSTNAME} ###"
echo "### Started at: ${start_time} ###"
echo

source workflow/gatk_variables.sh
BAM=${WORK}/bam/Sample_${SGE_TASK_ID}/Aligned_split.bam

BAMFILE=$(basename ${BAM})
# move bamfile and index to localscratch
cp ${BAM}* ${TMPDIR}
#cp ${BAM%.bam}.bai $TMPDIR

GATK_JAVA_ARGS='-Xmx96g -Xms96g -XX:-UseLargePages' 
java ${GATK_JAVA_ARGS} -jar ${GATK_ROOT}/GenomeAnalysisTK.jar \
   -T IndelRealigner \
   --maxReadsInMemory 1000000 \
   --maxReadsForRealignment 1000000 \
   -R ${HG19} \
   -I ${TMPDIR}/${BAMFILE} \
   -targetIntervals ${WORK}/realigner/Sample_${SGE_TASK_ID}.intervals \
   -known ${GATK_BUNDLE}/Mills_and_1000G_gold_standard.indels.hg19.vcf \
   -known ${GATK_BUNDLE}/1000G_phase1.indels.hg19.vcf \
   -o $TMPDIR/${BAMFILE%_split.bam}_realigned.bam

mv $TMPDIR/${BAMFILE%_split.bam}_realigned.bam ${WORK}/bam/Sample_${SGE_TASK_ID}

end_time=$(date)
end_secs=$(date +%s)
time_elapsed=$(echo "${end_secs} - ${start_secs}" | bc)
echo
echo "### Ended at: ${end_time} ###"
echo "### Time Elapsed: ${time_elapsed} ###"
