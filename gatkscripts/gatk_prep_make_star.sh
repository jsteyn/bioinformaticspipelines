#!/bin/bash
#$ -cwd -V
#$ -pe smp 10
#$ -l h_vmem=40G
# $ -M email@email.com
#$ -m a
#$ -N AAAstarprep
#$ -o /home/nsjc11/WORKING_DATA/workdir/logs/starprep.$JOB_ID.$TASK_ID.out
#$ -e /home/nsjc11/WORKING_DATA/workdir/logs/starprep.$JOB_ID.$TASK_ID.err

module add apps/STAR/2.4.0j

start_time=$(date)
start_secs=$(date +%s)
echo "### Logging Info ###"
echo "### Job: ${JOB_ID}: ${JOB_NAME} ###"
echo "### Array ID: ${SGE_TASK_ID} ###"
echo "### Job Occupying ${NSLOTS} slots ###"
echo "### Job running on ${HOSTNAME} ###"
echo "### Started at: ${start_time} ###"
echo

WORK=/home/nsjc11/WORKING_DATA/workdir

HG19=/opt/databases/GATK_bundle/2.8/hg19/ucsc.hg19.fasta

# 1) STAR uses genome index files that must be saved in unique directories. The human genome index was built from the FASTA file hg19.fa as follows:
GENOME=${WORK}/hg19
mkdir ${GENOME}
STAR --runMode genomeGenerate --genomeDir ${GENOME} \
    --genomeFastaFiles ${HG19} \
    --runThreadN ${NSLOTS}

echo
echo "### Ended at: ${end_time} ###"
echo "### Time Elapsed: ${time_elapsed} ###"
