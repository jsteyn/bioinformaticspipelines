# README #

This repository contains generic pipelines for various bioinformatics tasks.

The pipelines consist of bash scripts that are to be submitted as array jobs to a SGE cluster.

I have a basic directory structure that I work from. This usually is:
```
\MY_WORKING_DIRECTORY\bam
\MY_WORKING_DIRECTORY\bed
\MY_WORKING_DIRECTORY\coord_counts
\MY_WORKING_DIRECTORY\coords
\MY_WORKING_DIRECTORY\fastq
\MY_WORKING_DIRECTORY\fastq_trimmed
\MY_WORKING_DIRECTORY\fastqc
\MY_WORKING_DIRECTORY\fastqc_trimmed
\MY_WORKING_DIRECTORY\logs
\MY_WORKING_DIRECTORY\R
\MY_WORKING_DIRECTORY\scripts
```
I adopted a naming convention for my scripts. Scripts starting with "prep" are preparatory and are only needed the first time the sequence of scripts are run. For instance when preparing a genome for STAR, doing trimming or quality checking on fastq files.

Scripts starting with "de" is for differential gene expression. The digits after the de is for ordering the files according to the sequence in which they will be executed. By using the digits the files are ordered by default when you view a listing of the directory. However the naming of the files have no effect on the order in which the files are executed. 

If the files are submitted to the queue in order, they will be executed in order according to the array job switches that I used in the headers of the scripts.

There are two files that are required by all scripts for setting samplenames and paths. These two files are samples.txt and variables.txt. There are examples of these two files in the "preparatory" directory of the repository.

