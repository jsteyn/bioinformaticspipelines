#!/bin/bash
###########################
# qsub switches
###########################
#$ -l h_vmem=70G
#$ -pe smp 1
#$ -M jannetta.steyn@newcastle.ac.uk
#$ -m b
#$ -cwd
#$ -j y
###########################
module load apps/plink/1.07/gcc-4.4.6
BASE_DIR='/users/data/jannetta/SNP_Array_A2311/'
BFILE=${BASE_DIR}BED/flippedDataPlusHapMap
OUT=${BASE_DIR}BED/SNPs1
EXCLUDE=${BASE_DIR}BED/merged_flippedDataPlusHapMap.missnp
plink --noweb --bfile ${BFILE} --exclude ${EXCLUDE} --make-bed --out ${OUT}

