#!/bin/bash
###########################
# qsub switches
###########################
#$ -l h_vmem=70G
#$ -pe smp 1
#$ -M jannetta.steyn@newcastle.ac.uk
#$ -m b
#$ -cwd
#$ -j y
###########################
module load apps/plink/1.07/gcc-4.4.6
HAPMAP=/users/data/HapMapPlinkfiles/hapmap_YRI_r23a_filtered
BASE_DIR='/users/data/jannetta/SNP_Array_A2311/'
EXTRACT=${BASE_DIR}BED/prunedlist.prune.in
EXCLUDE=${BASE_DIR}BED/range23-26.txt
OUT=${BASE_DIR}BED/YRIpruned
plink --noweb --allow-no-sex --bfile ${HAPMAP} --extract ${EXTRACT} --exclude ${EXCLUDE} --make-bed --out ${OUT}
