#!/bin/bash
###########################
# qsub switches
###########################
#$ -l h_vmem=70G
#$ -M jannetta.steyn@newcastle.ac.uk
#$ -m b
#$ -cwd
#$ -j y
#$ -N PoorSamples
###########################
module load apps/plink/1.07/gcc-4.4.6
source /users/njss3/data/jannetta/A2311_eQTL_analysis/scripts/variables.sh
BFILE=${BASE_DIR}PLINK/SexUpdatePedFile
OUT=${BASE_DIR}PLINK/QCdsamples
REMOVE=${BASE_DIR}PLINK/PoorCallRateSamples.txt
plink --noweb --bfile ${BFILE} --remove ${REMOVE} --make-bed --out ${OUT}
