import os
import glob

lst_files=glob.glob("A2311_*.txt")
lst_files=sorted(lst_files)
######################################################
# Create tfam file
######################################################
sample_file=open("A2311.tfam",'w')
inc = 0
for f in lst_files:
	inc = inc+1
	fh = open(f)
	print(f)
	line = fh.readline()
	pline = line.split("\t")
	plinelen = len(pline)
	for sample in range(23,plinelen,4):
		sample_name = pline[sample].split(".")
		sample_file.write(str(inc)+"\t"+sample_name[0]+"\t0\t0\t0\t1\n")
	fh.close()
sample_file.close()

######################################################
# Create SNP file
######################################################
snp_file = open("A2311.tped",'w')
#######################
# Open all files
#######################
fh = list() # file handles
for f in lst_files:
	fh.append(open(f))
	header = fh[len(fh)-1].readline() # read all the header lines to get them out of the way
	print(f)

for fh0_line in fh[0]: # for each line in file 0
	pline = fh0_line.split("\t")
	gtype_dict={'A':pline[5], 'B':pline[6]}
	hline = pline[2]+"\t"+pline[7]+"\t"+"0\t"+pline[17]+"\t"
	snp_file.write(hline)
	plinelen=len(pline)
	for sample in range(23,plinelen,4): # write samples for file0
		gtype=pline[sample]
		if gtype[0]=='N':
			snp_file.write("0\t0\t")
		else:
			snp_file.write(gtype_dict[gtype[0]]+"\t"+gtype_dict[gtype[1]]+"\t")
	for other_file in range(1,len(fh)): # read lines in other files
		line = fh[other_file].readline()
		pline = line.split("\t")
		plinelen=len(pline)
		for sample in range(23,plinelen,4): # write samples for other files
			gtype=pline[sample]
			if gtype[0]=='N':
				snp_file.write("0\t0\t")
			else:
				snp_file.write(gtype_dict[gtype[0]]+"\t"+gtype_dict[gtype[1]]+"\t")
			# snp_file.write(gtype[0]+"\t"+gtype[1]+"\t")
	snp_file.write("\n")
for f in fh: # close all files
	f.close()
	
snp_file.close()

			

