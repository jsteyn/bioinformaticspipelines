#!/bin/bash
###########################
# qsub switches
###########################
#$ -l h_vmem=70G
#$ -pe smp 1
#$ -M jannetta.steyn@newcastle.ac.uk
#$ -m b
#$ -cwd
#$ -j y
###########################
module load apps/plink/1.07/gcc-4.4.6
BASE_DIR=/users/data/jannetta/SNP_Array_A2311/
BFILE=${BASE_DIR}BED/flippedDataPlusHapMap
BMERGE=${BASE_DIR}BED/HapMapCEUYRIJPT
OUT=${BASE_DIR}BED/merged_flippedDataPlusHapMap
plink --bfile ${BFILE} --bmerge ${BMERGE}.bed ${BMERGE}.bim ${BMERGE}.fam --make-bed --noweb --out ${OUT}
