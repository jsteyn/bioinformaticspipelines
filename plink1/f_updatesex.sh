#!/bin/bash
###########################
# qsub switches
###########################
#$ -l h_vmem=20G
#$ -M jannetta.steyn@newcastle.ac.uk
#$ -m b
#$ -cwd
#$ -j y
#$ -N updatesexcheck
#$ -hold_jid sexcheck
###########################
module load apps/plink/1.07/gcc-4.4.6
source /users/njss3/data/jannetta/A2311_eQTL_analysis/scripts/variables.sh
FILENAME='A2311'
plink --noweb --bfile ${BASE_DIR}PLINK/${FILENAME} --allow-no-sex --update-sex ${BASE_DIR}PLINK/myfile.txt --make-bed --out ${BASE_DIR}PLINK/SexUpdatePedFile
