#!/bin/bash
###########################
# qsub switches
###########################
#$ -l h_vmem=70G
#$ -pe smp 1
#$ -M jannetta.steyn@newcastle.ac.uk
#$ -m b
#$ -cwd
#$ -j y
###########################
module load apps/plink/1.07/gcc-4.4.6
BASE_DIR='/users/data/jannetta/SNP_Array_A2311/'
BFILE=${BASE_DIR}BED/prunedsnps
OUT=${BASE_DIR}BED/familyexcluded
EXCLUDE=${BASE_DIR}BED/exclude_family.txt
plink --noweb --allow-no-sex --bfile ${BFILE} --remove ${EXCLUDE} --make-bed --out ${OUT}
