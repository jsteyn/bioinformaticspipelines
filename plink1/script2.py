import re
import csv

f = open("E:\\work\\Mauro_eQTL\\AllHumanLociP10-9_.txt")
fw = open("E:\\work\\Mauro_eQTL\\out.txt",'w+')
log = open("E:\\work\\Mauro_eQTL\\log.txt",'w+')
notonchipfile = open("E:\\work\\Mauro_eQTL\\notonchip.txt",'w+')
csvreader = csv.reader(f,delimiter='\t')
outwriter = csv.writer(fw,delimiter='\t')
counter=0
inclcount=0
omitcount=0
pluscount=0
header = csvreader.next()
uniqueIDs = dict()
# FOR EACH LINE I CSV FILE
for row in csvreader:
    for item in row:
            item=item.replace('\n','')
            item=item.replace('\r','')
    counter=counter+1
    match = re.search('rs[0-9]+',row[20])
    plusmatch = re.search('[\+:x]',row[20])
    # IF COLUMN 20 (i.e. U IN EXCEL) DOES NOT HAVE rs ID, THEN EXCLUDE
    # IF COLUMN 20 (i.e. U IN EXCEL) CONTAINS +,: or x, THEN EXCLUDE
    if match is None or plusmatch is not None:
       if plusmatch is not None:
           pluscount = pluscount + 1
       log.write("Exclude row: " + row[20] + "\n")
       #print("Exclude row: " + row[20])
       omitcount = omitcount+1
    else:
        # SPLIT COLUMN 21 (i.e. V IN EXCEL)
        rsIDs=row[21].split(",")
        inclcount=inclcount+1
        # IF MORE THAN ONE rs THEN WRITE A LINE FOR EACH rs
        if len(rsIDs) > 1:
            for rsID in rsIDs:
                row[21]=rsID.strip()
                outwriter.writerow(row)
                uniqueIDs[row[21]]=0
                log.write("Split line " + str(counter) + ": " + row[20] + "\t" + row[21] + "\n")
                #print("Split line " + str(counter) + ": " + row[20] + "\t" + row[21])
        # ROW WITH SINGLE rs IN COLUMN V, JUST WRITE AS IS
        else:
            outwriter.writerow(row)
            uniqueIDs[row[21]]=0
    
print("Rows in spreadsheet: " + str(counter))
print("Included rows: " + str(inclcount))
print("Omitted rows: " + str(omitcount))
print("Number of rows omitted with + in rsID column: " + str(pluscount))
print("Unique rs IDs in excel file: " + str(uniqueIDs.__len__()))

# Read rs IDs available on chip
# GET LIST OF IDS ON CHIP
rsIDsonChip=dict()
rsfile=open("E:\\work\\Mauro_eQTL\\rsIDSonChip.txt")
blankrs=0
for rs in rsfile:
    # IF MORE THAN ONE rs ID IN THIS POSITION, CREATE NEW ENTRY FOR EACH
    if not rs.strip()==".":
        rsIDs=rs.strip().split(",")
        for anID in rsIDs:
            rsIDsonChip[anID]=0
    # IF RS EMPTY THEN EXCLUDE
    else:
        blankrs = blankrs+1
print("Lines in chip file: " + str(rsIDsonChip.__len__()))
print("Blank rsIDs: " + str(blankrs))

# Check through uniqueIDs to see which are in rsIDsonChip
notonchip=0
for rs in uniqueIDs:
    if rsIDsonChip.has_key(rs):
        uniqueIDs[rs]=1
for rs in uniqueIDs:
    if uniqueIDs[rs]==0:
        notonchip=notonchip+1
        notonchipfile.write(rs + "\n")
print("Number of rs not on chip: " + str(notonchip))
notonchipfile.close()
rsfile.close()
log.close()
fw.close()
f.close()