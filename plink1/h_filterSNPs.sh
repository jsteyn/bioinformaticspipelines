#!/bin/bash
###########################
# qsub switches
###########################
#$ -l h_vmem=70G
#$ -pe smp 1
#$ -M jannetta.steyn@newcastle.ac.uk
#$ -m b
#$ -cwd
#$ -j y
###########################
module load apps/plink/1.07/gcc-4.4.6
BASE_DIR='/users/data/jannetta/SNP_Array_A2311/'
BFILE=${BASE_DIR}BED/QCdsamples
OUT=${BASE_DIR}BED/freqsnps
EXCLUDE=${BASE_DIR}BED/range23-26.txt
plink --noweb --make-bed --allow-no-sex --bfile ${BFILE} --maf 0.1 --geno 0.05 --hwe 0.00000001 --exclude ${EXCLUDE} --out ${OUT}
