#!/bin/bash -e
#$ -l h_vmem=10G
#$ -pe smp 1
#$ -j y
# $ -M email@email.com
#$ -m b
#$ -cwd
module load apps/bowtie2/2.1.0
module load apps/tophat/2.0.10/gcc-4.4.6+samtools-0.1.18+boost-1.49.0
module load apps/java
FASTQ_DIR='/users/njss3/data/jannetta/Aurora_RNAseq/OUTPUT/'
BAMDIR='/users/njss3/data/jannetta/Aurora_RNAseq/bamfiles/'
BAMSORTEDDIR='/users/njss3/data/jannetta/Aurora_RNAseq/bamsortedfiles/'
FILENUMBER=$(printf "%02d" ${SGE_TASK_ID})
 PFC_JW_24_ACTTGA_L008_R2_001.QCed.JSS.fastq
FILENAME_A=`ls ${FASTQ_DIR}PFC_JW_${FILENUMBER}_*_R1_001.QCed.JSS.fastq`
FILENAME_B=`ls ${FASTQ_DIR}PFC_JW_${FILENUMBER}_*_R2_001.QCed.JSS.fastq`
FILENAME_C=`basename ${FILENAME_A}`
FILENAME_C=${FILENAME_C%.fastq}
#D. SORT BAM FILES
samtools sort -n ${BAMDIR}${FILENAME_C}.bam ${BAMSORTEDDIR}${FILENAME_C}.sorted
