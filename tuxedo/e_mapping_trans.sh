#!/bin/bash -e
###########################
# qsub switches
###########################
#$ -l h_vmem=10G
#$ -pe smp 1
#$ -j y
# $ -M email@email.com
#$ -m a
#$ -cwd -V
###########################
# Load modules
###########################
module load apps/cufflinks/2.1.1
module load apps/bowtie2/2.1.0
module load apps/samtools/1.1
module load  libs/boost/1.45.0
module load apps/tophat/2.0.11
module load apps/java
###########################
# Constants
###########################
SLOT_NUM=1
export BOWTIE2_INDEXES='/users/data/Bowtie_Ref/hg19/'
BT2_BASE='hg19'
export BOWTIE2_INDEXES='/users/data/Bowtie_Ref/hg19/GENCODE.V19/'
TRANS_BASE='gencode.v19.annotation'
INPUT_DIR='/users/njss3/data/jannetta/Aurora_RNAseq/OUTPUT/'
OUTPUT_DIR='/users/njss3/data/jannetta/Aurora_RNAseq/samfiles2/'
###########################
# SGE_TASK_ID takes value from -t switch on qsub command line
###########################
FILENUMBER=$(printf "%02d" ${SGE_TASK_ID})
FILENAME_A=`ls ${INPUT_DIR}PFC_JW_${FILENUMBER}_*R1_001.QCed.JSS.fastq`
FILENAME_B=`ls ${INPUT_DIR}PFC_JW_${FILENUMBER}_*R2_001.QCed.JSS.fastq`
###########################
# Use basename to extract only filename from full path and file
###########################
FILENAME_C=`basename ${FILENAME_A}`
###########################
# Remove fastq extention to get a base filename
###########################
FILENAME_C=${FILENAME_C%.fastq}
###########################
# Run bowtie - create .sam file
###########################
bowtie2 \
   -p ${SLOT_NUM} \
   -x ${TRANS_BASE} \
   -1 ${FILENAME_A} \
   -2 ${FILENAME_B} \
   -S ${OUTPUT_DIR}${FILENAME_C}.sam

