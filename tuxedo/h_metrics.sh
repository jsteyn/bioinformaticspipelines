#!/bin/bash -e
###########################
# qsub switches
###########################
#$ -l h_vmem=10G
#$ -pe smp 1
#$ -j y
# $ -M email@email.com
#$ -m a
#$ -cwd
###########################
# Load modules
###########################
module load apps/bowtie2
module load apps/samtools
module load  libs/boost
module load apps/tophat
module load apps/java
module add apps/picard
module load apps/R
###########################
# Constants
###########################
SLOT_NUM=1
INPUT_DIR='/users/njss3/data/jannetta/Aurora_RNAseq/bamsortedfiles/'
OUTPUT_DIR='/users/njss3/data/jannetta/Aurora_RNAseq/metrics/'
TMPDIR='/users/njss3/data/jannetta/Aurora_RNAseq/TMPDIR/'
###########################
# SGE_TASK_ID takes value from -t switch on qsub command line
###########################
FILENUMBER=$(printf "%02d" ${SGE_TASK_ID})
FILENAME_A=`ls ${INPUT_DIR}PFC_JW_${FILENUMBER}_*R1_001.QCed.JSS.sorted.bam`
FILENAME_B=`ls ${INPUT_DIR}PFC_JW_${FILENUMBER}_*R2_001.QCed.JSS.sorted.bam`
###########################
# Use basename to extract only filename from full path and file
###########################
FILENAME_C=`basename ${FILENAME_A}`
###########################
# Remove fastq extention to get a base filename
###########################
FILENAME_C=${FILENAME_C%.fastq}
###########################
# Create an output directory for tophat output
###########################
TOPHAT_OUTPUT_DIR='${OUTPUT_DIR}${FILENAME_C}_tophat_out/'
###########################
# Do metrics
###########################
java -Xmx6g -XX:-UseLargePages -Djava.io.tmpdir=$TMPDIR -jar $PICARDDIR/java/CollectInsertSizeMetrics.jar INPUT=$INPUT_DIR${FILENAME_C} HISTOGRAM_FILE=$OUTPUT_DIR${FILENAME_C}_hist.pdf OUTPUT=$OUTPUT_DIR${FILENAME_C}.txt
