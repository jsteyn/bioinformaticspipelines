#! /bin/bash -e
#$ -cwd
#$ -j y -V
#$ -l mem_free=10G,h_vmem=15G
#$ -pe smp 1
# $ -M email@email.com
#$ -m e
#/********************** on lampredi2
export _JAVA_OPTIONS="-XX:-UseLargePages"
SCRATCH_DIR=$TMPDIR
module add apps/python/2.7.3/gcc-4.4.6
module add apps/htseq/0.6.1p1/gcc-4.4.6+python-2.7.3+numpy-1.6.2
module add apps/seqtk/1.0/gcc-4.4.6
module add libs/numpy/1.6.2/gcc-4.4.6+python-2.7.3+atlas-3.10.0
module add libs/pysam/0.7.7/gcc-4.4.6+python-2.7.3
###########################
# Constants
###########################
SLOT_NUM=1
BASE_DIR='/users/data/jannetta/Aurora_RNAseq/'
GTF_FILE='/users/data/Bowtie_Ref/hg19/GENCODE.V19/gencode.v19.annotation.gtf'
TMP_DIR=${BASE_DIR}TMPDIR/
INPUT_DIR=${BASE_DIR}tophat/
OUTPUT_DIR=${BASE_DIR}counts/
###############
# SGE_TASK_ID takes value from -t switch on qsub command line
###########################
FILENUMBER=$(printf "%02d" ${SGE_TASK_ID})
FILENAME_A=`ls ${BASE_DIR}fastq/PFC_JW_${FILENUMBER}_*R1_001.fastq`
FILENAME_B=`ls ${BASE_DIR}fastq/PFC_JW_${FILENUMBER}_*R2_001.fastq`
###########################
# Use basename to extract only filename from full path and file
###########################
FILENAME_C=`basename ${FILENAME_A}`
###########################
# Remove fastq extention to get a base filename
###########################
FILENAME_C=${FILENAME_C%.fastq}
###########################
# Create an output directory for htseq output
###########################

OUT_COUNT="${OUTPUT_DIR}${FILENAME_C}.htseq.count.union.txt"
OUT_COUNT_STRANDED="${OUTPUT_DIR}${FILENAME_C}.htseq.reverse_stranded.count.union.txt"


#for tophat alignment mapQ > 3 means unique alignment
python -m HTSeq.scripts.count -f bam -r name -s no -a 4 -t exon -i gene_id -m union \
   "${INPUT_DIR}${FILENAME_C}_tophat_out/accepted_hits.bam" \
   $GTF_FILE \
   > $OUT_COUNT

python -m HTSeq.scripts.count -f bam -r name -s reverse -a 4 -t exon -i gene_id -m union \
   "${INPUT_DIR}${FILENAME_C}_tophat_out/accepted_hits.bam" \
   $GTF_FILE \
   > $OUT_COUNT_STRANDED

