#!/bin/bash -e
###########################
# qsub switches
###########################
#$ -l h_vmem=10G
#$ -pe smp 2
#$ -j y
# $ -M email@email.com
#$ -m a
#$ -cwd
###########################
# Load modules
###########################
module load apps/bowtie2/2.2.4/gcc-4.4.6
module load apps/samtools

samtools index ../bamsortedfiles/PFC_JW_01_TTAGGC_L005_R1_001.QCed.JSS.sorted.bam
