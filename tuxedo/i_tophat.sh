#!/bin/bash -e
###########################
# qsub switches
###########################
#$ -l h_vmem=10G
#$ -pe smp 2
#$ -j y
# $ -M email@email.com
#$ -m a
#$ -cwd
###########################
# Load modules
###########################
module load apps/bowtie2/2.2.4/gcc-4.4.6 
module load apps/samtools
module load apps/tophat/2.0.13/gcc-4.4.6+boost-1.57.0
module load apps/java
###########################
# Constants
###########################
SLOT_NUM=2
BASE_DIR='/users/data/jannetta/Aurora_RNAseq/'
REFERENCE_IN='/users/data/Bowtie_Ref/hg19/'
TMPDIR=${BASE_DIR}TMPDIR/
BT2_BASE=${REFERENCE_IN}hg19
TRANS_BASE='/users/data/Bowtie_Ref/hg19/GENCODE.V19/gencode.v19.annotation'
INPUT_DIR=${BASE_DIR}fastq/
OUTPUT_DIR=${BASE_DIR}tophat/
METRICS_DIR=${BASE_DIR}metrics/
###########################
# SGE_TASK_ID takes value from -t switch on qsub command line
###########################
FILENUMBER=$(printf "%02d" ${SGE_TASK_ID})
FILENAME_A=`ls ${INPUT_DIR}PFC_JW_${FILENUMBER}_*R1_001.fastq`
FILENAME_B=`ls ${INPUT_DIR}PFC_JW_${FILENUMBER}_*R2_001.fastq`
###########################
# Use basename to extract only filename from full path and file
###########################
FILENAME_C=`basename ${FILENAME_A}`
###########################
# Remove fastq extention to get a base filename
###########################
FILENAME_C=${FILENAME_C%.fastq}
###########################
# Create an output directory for tophat output
###########################
TOPHAT_OUTPUT_DIR=${OUTPUT_DIR}${FILENAME_C}_tophat_out/
###########################
# Extract MAX_INSERT_SIZE and STANDARD_DEVIATION from txt file created by CollectInsertSizeMetrics (Picard)
###########################
DATALINE=`cat ${METRICS_DIR}/${FILENAME_C}.QCed.JSS.sorted.bam.txt|grep -n ".*"|grep "\(^8:\)"`
READ_LENGTH=99
SD=`head -8 ${METRICS_DIR}/${FILENAME_C}.QCed.JSS.sorted.bam.txt | tail -1 | cut -f 6`
SD=`printf "%.f" $SD`
echo "${FILENAME_A} SD: ${SD}"
MID=`head -8 ${METRICS_DIR}/${FILENAME_C}.QCed.JSS.sorted.bam.txt | tail -1 | cut -f 5`
MATE_INNER_DIST=$(echo "${MID} - (2*${READ_LENGTH})" | bc)
MID=`printf "%.f" $MATE_INNER_DIST`
echo "${FILENAME_A} MID: ${MID}"
###########################
# Run tophat2
###########################
# ${TOPHATDIR}/bin/tophat \
#   --output-dir $TOPHAT_OUTPUT_DIR \
#   --num-threads ${SLOT_NUM} \
#   --mate-inner-dist $MID \
#   --mate-std-dev $SD \
#   --library-type fr-firststrand \
#   --b2-very-sensitive \
#   --tmp-dir ${TMPDIR} \
#   --transcriptome-index ${TRANS_BASE} \
#   ${BT2_BASE} \
#   ${FILENAME_A} ${FILENAME_B}

${TOPHATDIR}/bin/tophat2 \
-p ${SLOT_NUM} \
--library-type fr-firststrand \
--read-realign-edit-dist 0 \
-o ${TOPHAT_OUTPUT_DIR} \
--min-segment-intron 20 \
--segment-length 25 \
--microexon-search \
--mate-inner-dist ${MID} \
--mate-std-dev ${SD} \
--b2-sensitive \
-g 1 \
--transcriptome-index ${TRANS_BASE} \
--transcriptome-max-hits 20 \
${BT2_BASE} \
${FILENAME_A} ${FILENAME_B}
