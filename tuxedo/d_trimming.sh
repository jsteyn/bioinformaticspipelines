#! /bin/bash -e
#$ -cwd
#$ -j y
#$ -l mem_free=10G,h_vmem=15G
#$ -pe smp 2
# $ -M email@email.com
#$ -m e
# #$ -p -1000

res1=$(date +%s.%N) # use to calculate whole run time of the job

##/********************** on lampredi2
module load apps/bowtie2/2.2.4/gcc-4.4.6
module load apps/tophat/2.0.13/gcc-4.4.6+boost-1.57.0
module load apps/cufflinks/2.2.1/gcc-4.4.6+boost-1.49.0+samtools-0.1.18+eigen-3.0.5
module load libs/numpy/1.6.2/gcc-4.4.6+python-2.7.3+atlas-3.10.0
module load libs/pysam/0.7.7/gcc-4.4.6+python-2.7.3
SCRATCH_DIR="/users/njss3/lustre/Temp" #use it not to use, it is a question
module load apps/htseq/0.6.1p1/gcc-4.4.6+python-2.7.3+numpy-1.6.2
module load apps/picard/1.85/noarch
PICARDDIR="$PICARDDIR/java"
module load apps/seqtk/1.0/gcc-4.4.6
module load apps/autoadapt/20140123/gcc-4.4.6+perl-5.18.0
export AUTOADAPTTMP=~/lustre/autoadapt_temp
module load apps/R
#**********************/
###########################
# Constants
###########################
INPUT_DIR='/users/njss3/data/jannetta/Aurora_RNAseq/fastq/'
OUTPUT_DIR='/users/njss3/data/jannetta/Aurora_RNAseq/OUTPUT/'
SCRIPT_DIR='/users/njss3/data/jannetta/Aurora_RNAseq/scripts/'
###########################
# SGE_TASK_ID takes value from -t switch on qsub command line
###########################
FILENUMBER=$(printf "%02d" ${SGE_TASK_ID})
FILENAME_A=`ls ${INPUT_DIR}PFC_JW_${FILENUMBER}_*R1_001.fastq`
FILENAME_B=`ls ${INPUT_DIR}PFC_JW_${FILENUMBER}_*R2_001.fastq`
FILENAME_C=`basename ${FILENAME_A}`
###########################
# Use basename to extract only filename from full path and file
###########################
FILENAME_C=`basename ${FILENAME_A}`
FILENAME_D=`basename ${FILENAME_B}`
###########################
# Remove fastq extention to get a base filename
###########################
FILENAME_C=${FILENAME_C%.fastq}
FILENAME_D=${FILENAME_D%.fastq}

TRIMMED_FASTQ_1="${OUTPUT_DIR}${FILENAME_C}.trimmed.JSS.fastq"
TRIMMED_FASTQ_2="${OUTPUT_DIR}${FILENAME_D}.trimmed.JSS.fastq"
TRIMMED_FASTQ_seqtk_1="${OUTPUT_DIR}${FILENAME_C}.trimmed.seqtk.JSS.fastq"
TRIMMED_FASTQ_seqtk_2="${OUTPUT_DIR}${FILENAME_D}.trimmed.seqtk.JSS.fastq"

QCed_FASTQ_1="${OUTPUT_DIR}${FILENAME_C}.QCed.JSS.fastq"
QCed_FASTQ_2="${OUTPUT_DIR}${FILENAME_D}.QCed.JSS.fastq"


seqtk trimfq -b 7 -e 5 ${FILENAME_A} > ${TRIMMED_FASTQ_seqtk_1}
seqtk trimfq -b 7 -e 5 ${FILENAME_B} > ${TRIMMED_FASTQ_seqtk_2}



        echo "Start to do auto cut adapt and filtering bases"
        autoadapt.pl --quality-cutoff 20 --minimum-length 20 ${TRIMMED_FASTQ_seqtk_1} ${TRIMMED_FASTQ_1} ${TRIMMED_FASTQ_seqtk_2} ${TRIMMED_FASTQ_2}
        echo "done!"

        echo "perl to remove poly-Ns"
        perl ${SCRIPT_DIR}/removeNtailsFromFastQ.pl --1 ${TRIMMED_FASTQ_1} --2 ${TRIMMED_FASTQ_2} --o1 ${QCed_FASTQ_1} --o2 ${QCed_FASTQ_2}

        echo "running fastqc on QCed fastqs"
        fastqc --nogroup --noextract --outdir ${OUTPUT_DIR} -t 2 ${QCed_FASTQ_1}
        fastqc --nogroup --noextract --outdir ${OUTPUT_DIR} -t 2 ${QCed_FASTQ_2}
        echo "done!"


