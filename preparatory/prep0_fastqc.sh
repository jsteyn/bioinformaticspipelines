# prep0_fastqc version 1
# Jannetta's bioinformatics scripts
# The purpose of this script is t run fastqc on all your sample files.
# The source file variables.sh, is included with values set for a few
# variables that will be used in the pipelines. Make sure these are set
# before you run the scripts. Of importance for this script is the value
# of WORK.
#
#!/bin/bash -e
#$ -cwd -V
#$ -pe smp 1
#$ -l h_vmem=40G
#$ -m a
#$ -N AAAfastqc
#$ -o /user/logs/fastqc.$JOB_ID.$TASK_ID.out
#$ -e /user/logs/fastqc.$JOB_ID.$TASK_ID.err

module load apps/fastqc

start_time=$(date)
start_secs=$(date +%s)
echo "### Logging Info ###"
echo "###�| Job: ${JOB_ID}: ${JOB_NAME} ###"
echo "### Array ID: ${SGE_TASK_ID} ###"
echo "###�| Job Occupying ${NSLOTS} slots ###"
echo "### Job running on ${HOSTNAME} ###"
echo "### Started at: ${start_time} ###"
echo

source /user/scripts/variables.sh
FILELIST1=`ls ${WORK}fastq/*.fastq.gz`

echo "fastqc -o ${WORK}fastqc/ ${FILELIST1} "
fastqc -o ${WORK}fastqc/ ${FILELIST1} 

end_time=$(date)
end_secs=$(date +%s)
time_elapsed=$(echo "${end_secs} - ${start_secs}" | bc)
echo
echo "### Ended at: ${end_time} ###"
echo "### Time Elapsed: ${time_elapsed} ###"
