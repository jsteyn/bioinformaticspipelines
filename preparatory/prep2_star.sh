#!/bin/bash
#$ -cwd -V
#$ -pe smp 5
#$ -l h_vmem=40G
#$ -m a
#$ -N starprep
#$ -o /user/logs/hg19_74.$JOB_ID.$TASK_ID.out
#$ -e /user/logs/hg19_74.$JOB_ID.$TASK_ID.err

module add apps/STAR/2.5.2b

start_time=$(date)
start_secs=$(date +%s)
echo "### Logging Info ###"
echo "### Job: ${JOB_ID}: ${JOB_NAME} ###"
echo "### Array ID: ${SGE_TASK_ID} ###"
echo "### Job Occupying ${NSLOTS} slots ###"
echo "### Job running on ${HOSTNAME} ###"
echo "### Started at: ${start_time} ###"
echo

source /user/scripts/variables.sh

# 1) STAR uses genome index files that must be saved in unique directories. The human genome index was built from the FASTA file hg19.fa as follows:

if [ ! -d ${GENDIR} ]; then
  mkdir ${GENDIR}
fi

STAR --runMode genomeGenerate --genomeDir ${GENDIR} \
  --genomeFastaFiles ${GENOME} \
  --sjdbGTFfile ${GTF} \
  --runThreadN ${NSLOTS} \
  --sjdbOverhang 74 

echo
echo "### Ended at: ${end_time} ###"
echo "### Time Elapsed: ${time_elapsed} ###"
