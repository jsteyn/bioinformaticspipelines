# prep1_trim.sh version 1
# Jannetta's bioinformatics scripts
# The purpose of this script is to trim the reads in the fastq files.
# To determine the appropriate lengths you need to look at the fastqc
# report prepared by the fastqc program (script prep0_fastqc)
#
#!/bin/bash -e
#$ -cwd -V
#$ -pe smp 1-5
#$ -l h_vmem=40G
#$ -M jannetta.steyn@newcastle.ac.uk
#$ -m a
#$ -N AAAtrim
#$ -o /user/logs/trim.$JOB_ID.$TASK_ID.out
#$ -e /user/logs/trim.$JOB_ID.$TASK_ID.err

module add apps/Trimmomatic/0.3.6

start_time=$(date)
start_secs=$(date +%s)
echo "### Logging Info ###"
echo "### Job: ${JOB_ID}: ${JOB_NAME} ###"
echo "### Array ID: ${SGE_TASK_ID} ###"
echo "### Job Occupying ${NSLOTS} slots ###"
echo "### Job running on ${HOSTNAME} ###"
echo "### Started at: ${start_time} ###"
echo

source /user/scripts/variables.sh


readarray -t FILENAME < ${WORK}scripts/samples.txt

FASTQ1=`(ls ${WORK}fastq/${FILENAME[$SGE_TASK_ID-1]}*_R1.fastq.gz)`
FASTQ2=`(ls ${WORK}fastq/${FILENAME[$SGE_TASK_ID-1]}*_R2.fastq.gz)`
BASENAME=`basename ${FILENAME[$SGE_TASK_ID-1]}`
FASTQtrimmed1=`basename ${FASTQ1}`
FASTQtrimmed1=${FASTQtrimmed1%.fastq.gz}
FASTQtrimmed2=`basename ${FASTQ2}`
FASTQtrimmed2=${FASTQtrimmed2%.fastq.gz}
HEADCROP=11
CROP=88
MINLEN=88


runDir=${TMPDIR}/${BASENAME}
mkdir $runDir
cd $runDir

echo java -Xmx4g -Xms4g -jar /opt/software/clustersoft/install/Trimmomatic/Trimmomatic-0.35/trimmomatic-0.35.jar PE ${FASTQ1} ${FASTQ2} ${WORK}fastq_trimmed/${FASTQtrimmed1}.trimmed.fastq.gz ${WORK}fastq_trimmed/${FASTQtrimmed1}.unpaired ${WORK}fastq_trimmed/${FASTQtrimmed2}.trimmed.fastq.gz ${WORK}fastq_trimmed/${FASTQtrimmed2}.unpaired ILLUMINACLIP: TruSeq3-PE.fa:2:30:10 HEADCROP:$HEADCROP MINLEN:$MINLEN
java -Xmx4g -Xms4g -jar /opt/software/clustersoft/install/Trimmomatic/Trimmomatic-0.35/trimmomatic-0.35.jar PE \
	${FASTQ1} ${FASTQ2} \
	${WORK}fastq_trimmed/${FASTQtrimmed1}.trimmed.fastq.gz \
	${WORK}fastq_trimmed/${FASTQtrimmed1}.unpaired \
	${WORK}fastq_trimmed/${FASTQtrimmed2}.trimmed.fastq.gz \
	${WORK}fastq_trimmed/${FASTQtrimmed2}.unpaired \
	ILLUMINACLIP:/opt/software/clustersoft/install/Trimmomatic/Trimmomatic-0.35/adapters/TruSeq3-PE.fa:2:30:10 \
	HEADCROP:$HEADCROP \
	CROP:$CROP \
	MINLEN:$MINLEN

mv $runDir ${WORK}fastq/

end_time=$(date)
end_secs=$(date +%s)
time_elapsed=$(echo "${end_secs} - ${start_secs}" | bc)
echo
echo "### Ended at: ${end_time} ###"
echo "### Time Elapsed: ${time_elapsed} ###"
