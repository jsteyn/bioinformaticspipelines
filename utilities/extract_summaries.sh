#! /bin/sh
while read sn; do fn=`ls ${sn}*_R1*.zip`; unzip -j ${fn} ${fn%.zip}/summary.txt; mv summary.txt ${sn}.txt;done < ../scripts/samples.txt
while read sn; do fn=`ls ${sn}*_R2*.zip`; unzip -j ${fn} ${fn%.zip}/summary.txt; mv summary.txt ${sn}.txt;done < ../scripts/samples.txt
while read sn; do fn=`ls ${sn}*_R1*.zip`; unzip -j ${fn} ${fn%.zip}/fastqc_data.txt; mv fastqc_data.txt ${sn}_fastqc_data.txt;done < ../scripts/samples.txt
while read sn; do fn=`ls ${sn}*_R2*.zip`; unzip -j ${fn} ${fn%.zip}/fastqc_data.txt; mv fastqc_data.txt ${sn}_fastqc_data.txt;done < ../scripts/samples.txt
