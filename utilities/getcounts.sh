#! /bin/sh

while read fn; do sn=`ls -d bam/${fn}*.trimmed`; cut -f 4 ${sn}/ReadsPerGene.out.tab > counts/${fn}.count;done < scripts/samples.txt
