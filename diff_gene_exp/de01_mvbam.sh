#!/bin/bash -e
#$ -cwd -V
#$ -pe smp 1
#$ -l h_vmem=40G
# $ -M name@domain.com
#$ -m a
#$ -N AAAmvbam
#$ -hold_jid AAAstar
#$ -o /user/logs/mvbam.$JOB_ID.$TASK_ID.out
#$ -e /user/logs/mvbam.$JOB_ID.$TASK_ID.err


start_time=$(date)
start_secs=$(date +%s)
echo "### Logging Info ###"
echo "### Job: ${JOB_ID}: ${JOB_NAME} ###"
echo "### Array ID: ${SGE_TASK_ID} ###"
echo "### Job Occupying ${NSLOTS} slots ###"
echo "### Job running on ${HOSTNAME} ###"
echo "### Started at: ${start_time} ###"
echo

source /user/scripts/variables.sh

readarray -t FILENAME < ${WORK}/scripts/samples.txt

mv ${WORK}/bam/${FILENAME[$SGE_TASK_ID-1]}_R1.trimmed/Aligned.*.bam ${WORK}/bam/${FILENAME[$SGE_TASK_ID-1]}.bam

end_time=$(date)
end_secs=$(date +%s)
time_elapsed=$(echo "${end_secs} - ${start_secs}" | bc)
echo
echo "### Ended at: ${end_time} ###"
echo "### Time Elapsed: ${time_elapsed} ###"

