#!/bin/bash -e
#$ -cwd -V
#$ -pe smp 1
#$ -l h_vmem=40G
# $ -M name@domain.com
#$ -m a
#$ -N AAAbam2bg
#$ -hold_jid AAAstar
#$ -o /user/logs/bam2bg.$JOB_ID.$TASK_ID.out
#$ -e /user/logs/bam2bg.$JOB_ID.$TASK_ID.err

start_time=$(date)
start_secs=$(date +%s)
echo "### Logging Info ###"
echo "### Job: ${JOB_ID}: ${JOB_NAME} ###"
echo "### Array ID: ${SGE_TASK_ID} ###"
echo "### Job Occupying ${NSLOTS} slots ###"
echo "### Job running on ${HOSTNAME} ###"
echo "### Started at: ${start_time} ###"
echo

module load apps/bedtools

source /user/scripts/variables.sh

readarray -t FILENAME < ${WORK}/scripts/samples.txt


echo "bedtools genomecov -ibam ${WORK}bam/${FILENAME[$SGE_TASK_ID-1]}.sorted.coord.bam -g ${CHROMSIZES} -bga -split > ${WORK}bed/${FILENAME[$SGE_TASK_ID-1]}.bedGraph"
bedtools genomecov -ibam ${WORK}bam/${FILENAME[$SGE_TASK_ID-1]}.sorted.coord.bam -g ${CHROMSIZES} -bga -split> ${WORK}bed/${FILENAME[$SGE_TASK_ID-1]}.bedGraph


end_time=$(date)
end_secs=$(date +%s)
time_elapsed=$(echo "${end_secs} - ${start_secs}" | bc)
echo
echo "### Ended at: ${end_time} ###"
echo "### Time Elapsed: ${time_elapsed} ###"
