#!/bin/bash -e
#$ -cwd -V
#$ -pe smp 1
#$ -l h_vmem=40G
#$ -M jannetta.steyn@newcastle.ac.uk
#$ -m a
#$ -N Asortbam
#$ -hold_jid Amvbam
#$ -o /user/logs/sortbam.$JOB_ID.$TASK_ID.out
#$ -e /user/logs/sortbam.$JOB_ID.$TASK_ID.err

module load apps/samtools/1.2

start_time=$(date)
start_secs=$(date +%s)
echo "### Logging Info ###"
echo "### Job: ${JOB_ID}: ${JOB_NAME} ###"
echo "### Array ID: ${SGE_TASK_ID} ###"
echo "### Job Occupying ${NSLOTS} slots ###"
echo "### Job running on ${HOSTNAME} ###"
echo "### Started at: ${start_time} ###"
echo

source /user/scripts/variables.sh

readarray -t FILENAME < ${WORK}/scripts/samples.txt

echo samtools sort -o ${WORK}bam/${FILENAME[$SGE_TASK_ID-1]}.sorted.bam -O bam -n ${WORK}bam/${FILENAME[$SGE_TASK_ID-1]}.bam -T ${TMPDIR}/samsorttmp
samtools sort -o ${WORK}bam/${FILENAME[$SGE_TASK_ID-1]}.sorted.bam -O bam -n ${WORK}bam/${FILENAME[$SGE_TASK_ID-1]}.bam -T ${TMPDIR}/samsorttmp

end_time=$(date)
end_secs=$(date +%s)
time_elapsed=$(echo "${end_secs} - ${start_secs}" | bc)
echo
echo "### Ended at: ${end_time} ###"
echo "### Time Elapsed: ${time_elapsed} ###"

