#!/bin/bash -e
#$ -cwd -V
#$ -pe smp 1-5
#$ -l h_vmem=100G
# $ -M name@domain.com
#$ -m a
#$ -N AAAstar
#$ -hold_jid genomeprep
#$ -o /user/logs/star.$JOB_ID.$TASK_ID.out
#$ -e /user/logs/star.$JOB_ID.$TASK_ID.err

module add apps/STAR/2.5.2b

start_time=$(date)
start_secs=$(date +%s)
echo "### Logging Info ###"
echo "### Job: ${JOB_ID}: ${JOB_NAME} ###"
echo "### Array ID: ${SGE_TASK_ID} ###"
echo "### Job Occupying ${NSLOTS} slots ###"
echo "### Job running on ${HOSTNAME} ###"
echo "### Started at: ${start_time} ###"
echo

source /user/scripts/variables.sh


readarray -t FILENAME < ${WORK}/scripts/samples.txt

FASTQ1=`(ls ${WORK}/fastq_trimmed/${FILENAME[$SGE_TASK_ID-1]}*_R1*.fastq.gz)`
FASTQ2=`(ls ${WORK}/fastq_trimmed/${FILENAME[$SGE_TASK_ID-1]}*_R2*.fastq.gz)`
BASENAME=`basename ${FASTQ1}`
BASENAME=${BASENAME%.fastq.gz}

runDir=${TMPDIR}/${BASENAME}
mkdir $runDir
cd $runDir

echo "STAR --genomeDir ${GENDIR}  --readFilesIn ${FASTQ1} ${FASTQ2}  --readFilesCommand gunzip -c  --runThreadN ${NSLOTS}  --outSAMtype BAM SortedByCoordinate"
STAR \
  --genomeDir ${GENDIR} \
  --readFilesIn ${FASTQ1} ${FASTQ2} \
  --readFilesCommand gunzip -c \
  --runThreadN ${NSLOTS} \
  --outSAMtype BAM Unsorted \
  --twopassMode Basic \
  --quantMode GeneCounts

mv $runDir ${WORK}/bam/.

end_time=$(date)
end_secs=$(date +%s)
time_elapsed=$(echo "${end_secs} - ${start_secs}" | bc)
echo
echo "### Ended at: ${end_time} ###"
echo "### Time Elapsed: ${time_elapsed} ###"
