#!/bin/bash -e
#$ -cwd -V
#$ -pe smp 1-5
#$ -l h_vmem=100G
#$ -m a
#$ -N AAAbam2bed
#$ -hold_jid AAAstar
#$ -o /user/logs/bam2bed.$JOB_ID.$TASK_ID.out
#$ -e /user/logs/bam2bed.$JOB_ID.$TASK_ID.err

start_time=$(date)
start_secs=$(date +%s)
echo "### Logging Info ###"
echo "### Job: ${JOB_ID}: ${JOB_NAME} ###"
echo "### Array ID: ${SGE_TASK_ID} ###"
echo "### Job Occupying ${NSLOTS} slots ###"
echo "### Job running on ${HOSTNAME} ###"
echo "### Started at: ${start_time} ###"
echo

module load apps/bedtools

source /user/scripts/variables.sh

readarray -t FILENAME < ${WORK}/scripts/samples.txt


echo "bedtools bamToBed -i ${WORK}bam/${FILENAME[$SGE_TASK_ID-1]}.sorted.coord.bam > ${WORK}bed/${FILENAME[$SGE_TASK_ID-1]}.bed"
bedtools bamtobed -i ${WORK}bam/${FILENAME[$SGE_TASK_ID-1]}.sorted.coord.bam > ${WORK}bed/${FILENAME[$SGE_TASK_ID-1]}.bed


end_time=$(date)
end_secs=$(date +%s)
time_elapsed=$(echo "${end_secs} - ${start_secs}" | bc)
echo
echo "### Ended at: ${end_time} ###"
echo "### Time Elapsed: ${time_elapsed} ###"
